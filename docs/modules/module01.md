#Module 1 : Gitlab


Dans le présent module, nous verrons comment, pas à pas, je me suis familiarisé avec Git. Le but est de créer une série de pages web servant à documenter mes avancées au sein de l'option **Architecture et Design** sur le modèle de la FabAcademy. Cet apprentissage se décomposera en différents modules. Celui-ci est le premier.  

Les pages web seront hébergées sur Gitlab. Ce site offre deux options :  
* version simplifiée : coder directement sur le site de Gitlab via l'éditeur ;
* version complète : faire un clone dans un dossier sur mon ordinateur des pages hébergées sur le site à l'aide de **GitBash**, un invite de commande. Je peux alors les modifier à l'aide du logiciel de traitement de texte **Atom**. GitBash me permettra de faire les liens entre ce dossier-clone et le dossier-origine. Toutes les modifications sont conservées et datées.

**Cette deuxième option offre l'énorme avantage de pouvoir revenir à posteriori sur les différentes modifications apportées aux documents**, ce qui peut être particulièrement pratique lorsque l'on travaille à plusieurs sur un projet, et que chacun apporte ses modifications au fur et à mesure. De plus Atom permet de visualiser en temps réel la page web codée en Mardown.

**1. cloner le Repository**
installer GitBash
s'identifier sur GitBash : _git config --global user.name "anthony.millour"_ et _git config --global user.email "anthony.millour"_ : créer le lien entre entre GitBash et notre compte sur le site Gitlab.

clé SSH

Un dossier personnel a, au préalable, été créé sur Gitlab par les enseignants de l'option à l'adresse suivante : https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/docs

![image1](images/1.jpg)

**2. coder les pages**
apprendre le langage Markdown
installer Atom
ouvrir le dossier-clone (Repository) sur Atom
apporter des modifications + les enregistrer localement

**3. exporter les modifs sur Gitlab**
utiliser GitBash pour les enregistrer sur le dossier-origine
(voir https://www.youtube.com/watch?v=TlQbEWHt3pY)

**4. consulter les modifs**
blablablaaa
